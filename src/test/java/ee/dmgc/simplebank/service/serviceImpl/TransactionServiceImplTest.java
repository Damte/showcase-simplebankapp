package ee.dmgc.simplebank.service.serviceImpl;

import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.domain.model.Transaction;
import ee.dmgc.simplebank.dto.AccountDTO;
import ee.dmgc.simplebank.dto.TransactionDTO;
import ee.dmgc.simplebank.exception.*;
import ee.dmgc.simplebank.repository.AccountRepository;
import ee.dmgc.simplebank.repository.CustomerRepository;
import ee.dmgc.simplebank.repository.TransactionRepository;
import ee.dmgc.simplebank.service.TransactionService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceImplTest {
    TransactionDTO transaction = new TransactionDTO();
    AccountDTO accountDTO = new AccountDTO();
    AccountDTO destinationAccountDTO = new AccountDTO();
    Account account = new Account();
    Account destinationAccount = new Account();
    Customer customer1 = new Customer();
    Customer customer2 = new Customer();
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TransactionRepository transactionRepository;

    @BeforeEach
    public void before() {
        customer1.setFirstName("Raul");
        customer1.setLastName("Gonzalez");
        customer1.setPersonalId("000000");
        customer2.setFirstName("Xabi");
        customer2.setLastName("Alonso");
        customer2.setPersonalId("111111");

        customerRepository.save(customer1);
        customerRepository.save(customer2);

        account.setActive(true);
        account.setCreatedAt(LocalDateTime.now());
        account.setUpdatedAt(LocalDateTime.now());
        account.setCurrentBalance(BigDecimal.valueOf(5000));
        account.setAccountNumber("ES" + String.valueOf(123));
        account.setCustomer(customer1);

        destinationAccount.setActive(true);
        destinationAccount.setCreatedAt(LocalDateTime.now());
        destinationAccount.setUpdatedAt(LocalDateTime.now());
        destinationAccount.setCurrentBalance(BigDecimal.valueOf(0));
        destinationAccount.setAccountNumber("ES" + String.valueOf(124));
        destinationAccount.setCustomer(customer2);

        accountRepository.save(account);
        accountRepository.save(destinationAccount);

        System.out.println("Before every test");
    }

    @AfterEach
    public void cleanUp() {
        accountRepository.delete(accountRepository.findAccountByAccountNumber("ES123"));
        accountRepository.delete(accountRepository.findAccountByAccountNumber("ES124"));
        customerRepository.delete(customerRepository.findByPersonalId("000000"));
        customerRepository.delete(customerRepository.findByPersonalId("111111"));

        System.out.println("After every test");

    }

    @Test
    void shouldShowSimpleAssertion() {
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void testDoTransactionSuccess() throws AccountIsNotActive, ReachedTransferLimitPerMinute, AccountNotFound, ReachedTransferLimitPerDay, BalanceIsLessThanTransferAmount, AboveAmountLimit {
        //given:
        accountDTO.setAccountNumber("ES" + String.valueOf(123));
        destinationAccountDTO.setAccountNumber("ES" + String.valueOf(124));

        transaction.setDescription("Rent payment");
        transaction.setAccount(accountDTO);
        transaction.setTransactionAmount(BigDecimal.valueOf(5));
        transaction.setDestinationAccount(destinationAccountDTO);

        //then
        BigDecimal balanceBefore = account.getCurrentBalance();
        List<Account> modifiedAccounts = transactionService.doTransaction(transaction);
        BigDecimal balanceAfter = accountRepository.findAccountByAccountNumber("ES" + String.valueOf(123)).getCurrentBalance();
        List<Transaction> allTransactions = transactionRepository.findAll();
        Transaction transactionRecorded = allTransactions.stream()
                .filter(tr -> tr.getDescription() == "Rent payment")
                .reduce((a, b) -> {
                    throw new IllegalStateException("Multiple elements: " + a + ", " + b);
                })
                .get();
        //when
        Assert.assertNotNull(modifiedAccounts);
        Assert.assertTrue(allTransactions.size() > 0);
        assertEquals("Rent payment", transactionRecorded.getDescription());
        assertEquals(BigDecimal.valueOf(5).stripTrailingZeros(), transactionRecorded.getTransactionAmount().stripTrailingZeros());
        assertTrue(balanceBefore.compareTo(balanceAfter) > 0);
    }

    @Test
    public void testDoTransactionAboveLimitFails() {
        accountDTO.setAccountNumber("ES" + String.valueOf(123));
        destinationAccountDTO.setAccountNumber("ES" + String.valueOf(124));
        ;

        transaction.setDescription("Rent payment");
        transaction.setAccount(accountDTO);
        transaction.setTransactionAmount(BigDecimal.valueOf(10001));
        transaction.setDestinationAccount(destinationAccountDTO);

        Assertions.assertThrows(AboveAmountLimit.class, () -> {
            transactionService.doTransaction(transaction);
        });
    }

    @Test
    public void testIfTwoTransfersAreDoneInLessThanOneMinute() throws AccountIsNotActive, ReachedTransferLimitPerMinute, AccountNotFound, ReachedTransferLimitPerDay, BalanceIsLessThanTransferAmount, AboveAmountLimit {
        accountDTO.setAccountNumber("ES" + String.valueOf(123));
        destinationAccountDTO.setAccountNumber("ES" + String.valueOf(124));
        ;

        transaction.setDescription("Rent payment");
        transaction.setAccount(accountDTO);
        transaction.setTransactionAmount(BigDecimal.valueOf(5));
        transaction.setDestinationAccount(destinationAccountDTO);
        transactionService.doTransaction(transaction);

        TransactionDTO transaction2 = new TransactionDTO();
        transaction2.setDescription("Rent payment");
        transaction2.setAccount(accountDTO);
        transaction2.setTransactionAmount(BigDecimal.valueOf(5));
        transaction2.setDestinationAccount(destinationAccountDTO);

        Assertions.assertThrows(ReachedTransferLimitPerMinute.class, () -> {
            transactionService.doTransaction(transaction2);
        });
    }

    @Test
    public void testDoTransactionIfAccountDoesntExistFails() {
        accountDTO.setAccountNumber("ES" + String.valueOf(123));
        destinationAccountDTO.setAccountNumber("ES" + String.valueOf(125));
        ;

        transaction.setDescription("Rent payment");
        transaction.setAccount(accountDTO);
        transaction.setTransactionAmount(BigDecimal.valueOf(5));
        transaction.setDestinationAccount(destinationAccountDTO);

        Assertions.assertThrows(AccountNotFound.class, () -> {
            transactionService.doTransaction(transaction);
        });
    }

    @Test
    public void testDoTransactionIfAccountIsInactive() {
        accountDTO.setAccountNumber("ES" + String.valueOf(123));
        destinationAccountDTO.setAccountNumber("ES" + String.valueOf(124));

        account.setActive(false);
        accountRepository.save(account);

        transaction.setDescription("Rent payment");
        transaction.setAccount(accountDTO);
        transaction.setTransactionAmount(BigDecimal.valueOf(5));
        transaction.setDestinationAccount(destinationAccountDTO);

        Assertions.assertThrows(AccountIsNotActive.class, () -> {
            transactionService.doTransaction(transaction);
        });
    }

    @Test
    public void testDoTransactionWhenNotEnoughMoney() {
        accountDTO.setAccountNumber("ES" + String.valueOf(123));
        destinationAccountDTO.setAccountNumber("ES" + String.valueOf(124));

        account.setCurrentBalance(BigDecimal.ZERO);
        accountRepository.save(account);

        transaction.setDescription("Rent payment");
        transaction.setAccount(accountDTO);
        transaction.setTransactionAmount(BigDecimal.valueOf(5));
        transaction.setDestinationAccount(destinationAccountDTO);

        Assertions.assertThrows(BalanceIsLessThanTransferAmount.class, () -> {
            transactionService.doTransaction(transaction);
        });
    }
}
