package ee.dmgc.simplebank;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DMGCSimpleBankingApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(DMGCSimpleBankingApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

    }

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("ee.backoffice.simplebank")).build();
    }
}

