package ee.dmgc.simplebank.repository;

import ee.dmgc.simplebank.domain.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("FROM Account AS a WHERE a.customer.id=:id")
    List<Account> findAllByCustomerId(@Param("id") Long customerId);

    @Query("FROM Account AS a WHERE a.accountNumber=:number")
    Account findAccountByAccountNumber(@Param("number") String accountNumber);


}
