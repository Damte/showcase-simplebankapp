package ee.dmgc.simplebank.repository;

import ee.dmgc.simplebank.domain.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("FROM Transaction AS t WHERE t.account.id=:id OR t.destinationAccount.id=:id")
    List<Transaction> findAllByAccount_Id(@Param("id")Long id);

    @Query("FROM Transaction AS t WHERE t.transactionDateTime>:date AND  t.account.accountNumber=:aNumber")
    List<Transaction> findTransactionsDoneCurrentDayFromAccount(@Param("date")LocalDateTime date, @Param("aNumber") String aNumber);

    @Query(nativeQuery = true, value = "SELECT * FROM Transaction AS t ORDER BY t.transaction_Id desc LIMIT  1")
    Transaction findTopByTransactionIdOrderByTransactionDateTime();

}
