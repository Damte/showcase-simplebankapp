package ee.dmgc.simplebank.exception;

public class ReachedTransferLimitPerDay extends Throwable {

    public ReachedTransferLimitPerDay(String message) {
        super(message);
    }
}
