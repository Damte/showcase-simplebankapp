package ee.dmgc.simplebank.exception;

public class AccountNotFound extends Throwable {
    public AccountNotFound(String s) {
        super(s);
    }

}
