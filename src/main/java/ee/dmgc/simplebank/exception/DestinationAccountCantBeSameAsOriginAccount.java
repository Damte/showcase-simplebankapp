package ee.dmgc.simplebank.exception;

public class DestinationAccountCantBeSameAsOriginAccount extends Throwable {
    public DestinationAccountCantBeSameAsOriginAccount(String s) {
        super(s);
    }

}
