package ee.dmgc.simplebank.exception;

public class AboveAmountLimit extends Throwable {

    public AboveAmountLimit(String message) {
        super(message);
    }
}
