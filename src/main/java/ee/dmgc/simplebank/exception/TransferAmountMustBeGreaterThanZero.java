package ee.dmgc.simplebank.exception;

public class TransferAmountMustBeGreaterThanZero extends Throwable {
    public TransferAmountMustBeGreaterThanZero(String s) {
        super(s);
    }

}
