package ee.dmgc.simplebank.exception;

public class BalanceIsLessThanTransferAmount extends Throwable {
    public BalanceIsLessThanTransferAmount(String s) {
        super(s);
    }

}
