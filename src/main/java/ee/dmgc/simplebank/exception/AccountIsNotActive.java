package ee.dmgc.simplebank.exception;

public class AccountIsNotActive extends Throwable {
    public AccountIsNotActive(String s) {
        super(s);
    }

}
