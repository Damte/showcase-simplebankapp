package ee.dmgc.simplebank.exception;

public class ReachedTransferLimitPerMinute extends Throwable {

    public ReachedTransferLimitPerMinute(String message) {
        super(message);
    }
}
