package ee.dmgc.simplebank.exception;

public class CustomerNotFound extends Throwable {
    public CustomerNotFound(String s) {
        super(s);
    }

}
