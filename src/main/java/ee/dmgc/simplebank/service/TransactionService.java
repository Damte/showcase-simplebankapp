package ee.dmgc.simplebank.service;

import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.dto.TransactionDTO;
import ee.dmgc.simplebank.dto.TransactionDTOListTransactionsByAccount;
import ee.dmgc.simplebank.exception.*;

import java.util.List;

public interface TransactionService {
    List<Account> doTransaction(TransactionDTO transaction) throws AccountIsNotActive, BalanceIsLessThanTransferAmount, AboveAmountLimit, ReachedTransferLimitPerDay, ReachedTransferLimitPerMinute, AccountNotFound;

    List<TransactionDTOListTransactionsByAccount> findTransactionsByAccount(Long accountId);
}
