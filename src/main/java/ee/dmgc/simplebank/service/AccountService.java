package ee.dmgc.simplebank.service;

import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.exception.AccountIsNotActive;
import ee.dmgc.simplebank.exception.AccountNotFound;
import ee.dmgc.simplebank.exception.BalanceIsLessThanTransferAmount;
import ee.dmgc.simplebank.exception.CustomerNotFound;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {
    Account saveAccount(Long customerId) throws CustomerNotFound;

    List<Account> findAccountsByCustomer(Long customerId);

    Account findAccountById(Long accountId);

    Account findAccountByAccountNumber(String accountNumber);

    List<Account> updateBalanceAfterTransaction(String originAccountNumber, String destinyAccountNumber, BigDecimal amount) throws BalanceIsLessThanTransferAmount, AccountIsNotActive;

    void checkIfThereAreEnoughFunds(String originAccountNumber, BigDecimal amount) throws BalanceIsLessThanTransferAmount;

    void checkIfAccountsExistAndAreActive(String originAccountNumber, String destinyAccountNumber) throws AccountIsNotActive, AccountNotFound;

    Account setStatus(Long accountId);

}
