package ee.dmgc.simplebank.service;


import ee.dmgc.simplebank.domain.converter.CustomerConverter;
import ee.dmgc.simplebank.domain.generator.AccountGenerator;
import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.dto.CustomerDTOSignUp;
import ee.dmgc.simplebank.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private CustomerRepository customerRepository;


    @Autowired
    private CustomerConverter customerConverter;

    @Autowired
    private AccountGenerator accountGenerator;

    @Override
    public UserDetails loadUserByUsername(String personalId) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByPersonalId(personalId);
        if (customer == null) {
            throw new UsernameNotFoundException("User not found with username: " + customer);
        }
        return new org.springframework.security.core.userdetails.User(customer.getPersonalId(), customer.getPassword(),
                new ArrayList<>());
    }

    public Customer save(CustomerDTOSignUp customerDTO) {
        Customer newCustomer = customerConverter.dtoToCustomer(customerDTO);
        customerRepository.save(newCustomer);
        accountGenerator.generateAccount(newCustomer);


//		newCustomer.setCreatedAt(new Date());
////	newCustomer.setUpdatedAt(new Date());
        return newCustomer;
    }
}