package ee.dmgc.simplebank.service;

import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.exception.CustomerNotFound;

import java.util.List;

public interface CustomerService {
    Customer saveCustomer(Customer customer);

    List<Customer> findAllCustomers();

    List<Customer> findCustomersByFirstName(String firstName);

    Customer findCustomer(Long customerId) throws CustomerNotFound;

    Customer findCustomerByPersonalId(String personalId);

    Customer setStatus(Long customerId, String status);
}
