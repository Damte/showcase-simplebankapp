package ee.dmgc.simplebank.service.serviceImpl;

import ee.dmgc.simplebank.domain.generator.AccountGenerator;
import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.exception.AccountIsNotActive;
import ee.dmgc.simplebank.exception.AccountNotFound;
import ee.dmgc.simplebank.exception.BalanceIsLessThanTransferAmount;
import ee.dmgc.simplebank.exception.CustomerNotFound;
import ee.dmgc.simplebank.repository.AccountRepository;
import ee.dmgc.simplebank.service.AccountService;
import ee.dmgc.simplebank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountGenerator accountGenerator;
    private final CustomerService customerService;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, AccountGenerator accountGenerator, CustomerService customerService) {
        this.accountRepository = accountRepository;
        this.accountGenerator = accountGenerator;
        this.customerService = customerService;
    }

    @Override
    public Account saveAccount(Long customerId) throws CustomerNotFound {
        Account accountToBeSaved = new Account();
        accountToBeSaved.setAccountNumber(accountGenerator.generateAccountNumber());
        //At the moment I'm giving 50 complimentary EUR to any new customer in order to be able to perform some transactions.
        accountToBeSaved.setCurrentBalance(BigDecimal.valueOf(50));
        accountToBeSaved.setActive(true);
        accountToBeSaved.setCustomer(customerService.findCustomer(customerId));
        accountToBeSaved.setCreatedAt(LocalDateTime.now());
        accountToBeSaved.setUpdatedAt(LocalDateTime.now());
        accountRepository.save(accountToBeSaved);
        return accountToBeSaved;
    }

    @Override
    public List<Account> findAccountsByCustomer(Long customerId) {
        List<Account> accountList = accountRepository.findAllByCustomerId(customerId);
        return accountList;
    }

    @Override
    public Account findAccountById(Long accountId) {
        Account account = accountRepository.findById(accountId).get();
        return account;
    }

    @Override
    public Account findAccountByAccountNumber(String accountNumber) {
        Account account = accountRepository.findAccountByAccountNumber(accountNumber);
        return account;
    }

    @Override
    public List<Account> updateBalanceAfterTransaction(String originAccountNumber, String destinyAccountNumber, BigDecimal amount) {
        Account originAccount = accountRepository.findAccountByAccountNumber(originAccountNumber);
        Account destinyAccount = accountRepository.findAccountByAccountNumber(destinyAccountNumber);

        originAccount.setCurrentBalance(originAccount.getCurrentBalance().subtract(amount));
        originAccount.setUpdatedAt(LocalDateTime.now());
        destinyAccount.setCurrentBalance(destinyAccount.getCurrentBalance().add(amount));
        destinyAccount.setUpdatedAt(LocalDateTime.now());
        accountRepository.save(originAccount);
        accountRepository.save(destinyAccount);

        List<Account> accounts = new ArrayList<>();
        accounts.add(originAccount);
        accounts.add(destinyAccount);
        return accounts;
    }

    @Override
    public void checkIfThereAreEnoughFunds(String originAccountNumber, BigDecimal amount) throws BalanceIsLessThanTransferAmount {
        Account originAccount = accountRepository.findAccountByAccountNumber(originAccountNumber);
        if (originAccount.getCurrentBalance().compareTo(amount) < 0) {
            throw new BalanceIsLessThanTransferAmount("There are not enough funds on this account");
        }
    }

    @Override
    public void checkIfAccountsExistAndAreActive(String originAccountNumber, String destinyAccountNumber) throws AccountIsNotActive, AccountNotFound {
        Account originAccount = accountRepository.findAccountByAccountNumber(originAccountNumber);
        Account destinyAccount = accountRepository.findAccountByAccountNumber(destinyAccountNumber);
        if (destinyAccount == null) {
            throw new AccountNotFound("There is no account with the number: " + destinyAccountNumber);
        }
        if (!destinyAccount.getActive() || !originAccount.getActive()) {
            throw new AccountIsNotActive(originAccount.getActive() ?
                    destinyAccount.getAccountNumber() + " is not active"
                    : originAccount.getAccountNumber() + " is not active");
        }
    }


    @Override
    public Account setStatus(Long accountId) {
        Account account = accountRepository.findById(accountId).get();
        account.setActive(!account.getActive());
        account.setUpdatedAt(LocalDateTime.now());
        accountRepository.save(account);
        return account;
    }

}
