package ee.dmgc.simplebank.service.serviceImpl;

import ee.dmgc.simplebank.domain.converter.TransactionConverter;
import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.domain.model.Transaction;
import ee.dmgc.simplebank.dto.TransactionDTO;
import ee.dmgc.simplebank.dto.TransactionDTOListTransactionsByAccount;
import ee.dmgc.simplebank.exception.*;
import ee.dmgc.simplebank.repository.TransactionRepository;
import ee.dmgc.simplebank.service.AccountService;
import ee.dmgc.simplebank.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountService accountService;
    private final TransactionConverter transactionConverter;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, AccountService accountService, TransactionConverter transactionConverter) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
        this.transactionConverter = transactionConverter;
    }

    @Override
    public List<Account> doTransaction(TransactionDTO transaction) throws AccountIsNotActive, BalanceIsLessThanTransferAmount, AboveAmountLimit, ReachedTransferLimitPerDay, ReachedTransferLimitPerMinute, AccountNotFound {

        Transaction transactionMade = new Transaction();
        checkIfAmountIsAboveLimit(transaction.getTransactionAmount());
        checkIfLastTransactionWasMadeInTheLastMinute(transaction.getAccount().getAccountNumber());
        accountService.checkIfAccountsExistAndAreActive(transaction.getAccount().getAccountNumber(), transaction.getDestinationAccount().getAccountNumber());
        accountService.checkIfThereAreEnoughFunds(transaction.getAccount().getAccountNumber(), transaction.getTransactionAmount());
        checkIfMoreThanLimitNumberTransactionsWereMadeToday(transaction.getAccount().getAccountNumber(), 5);
        transactionMade.setDestinationAccount(accountService.findAccountByAccountNumber(transaction.getDestinationAccount().getAccountNumber()));
        transactionMade.setTransactionAmount(transaction.getTransactionAmount());
        transactionMade.setDescription(transaction.getDescription());
        transactionMade.setTransactionDateTime(LocalDateTime.now());
        transactionMade.setAccount(accountService.findAccountByAccountNumber(transaction.getAccount().getAccountNumber()));
        transactionRepository.save(transactionMade);
        List<Account> accountList = accountService.updateBalanceAfterTransaction(transaction.getAccount().getAccountNumber(), transaction.getDestinationAccount().getAccountNumber(), transaction.getTransactionAmount());
        return accountList;
    }

    private void checkIfAmountIsAboveLimit(BigDecimal amount) throws AboveAmountLimit {
        if (amount.compareTo(BigDecimal.valueOf(10000)) > 0) {
            throw new AboveAmountLimit("Transfer denied. For transfers greater than 10000EUR, please contact us by email");
        }
    }

    private void checkIfLastTransactionWasMadeInTheLastMinute(String originAccount) throws ReachedTransferLimitPerMinute {
        Transaction lastTransaction = transactionRepository.findTopByTransactionIdOrderByTransactionDateTime();
        if (lastTransaction == null) {
            return;
        }
        Boolean isOriginSame = originAccount.equals(lastTransaction.getAccount().getAccountNumber());
        if (LocalDateTime.now().minusMinutes(1).isBefore(lastTransaction.getTransactionDateTime()) && isOriginSame) {
            throw new ReachedTransferLimitPerMinute("You need to wait for a minute before doing a new transfer.");
        }
    }

    private void checkIfMoreThanLimitNumberTransactionsWereMadeToday(String accountNumber, Integer limit) throws ReachedTransferLimitPerDay {
        LocalDateTime minusOneDay = LocalDateTime.now().minusDays(1);
        List<Transaction> transactions = transactionRepository.findTransactionsDoneCurrentDayFromAccount(minusOneDay, accountNumber);
        if (transactions.size() >= limit) {
            throw new ReachedTransferLimitPerDay("You've done more than " + limit + " transfers today. Wait for 24 hours or consult us");
        }
    }


    @Override
    public List<TransactionDTOListTransactionsByAccount> findTransactionsByAccount(Long accountId) {

        List<Transaction> transactions = transactionRepository.findAllByAccount_Id(accountId);
        List<TransactionDTOListTransactionsByAccount> transactionDTOList = transactions.stream().map(transaction -> transactionConverter.transactionToDTO(transaction)).collect(Collectors.toList());
        return transactionDTOList;
    }
}
