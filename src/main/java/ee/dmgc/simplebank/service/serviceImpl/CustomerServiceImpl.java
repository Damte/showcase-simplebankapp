package ee.dmgc.simplebank.service.serviceImpl;

import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.domain.model.CustomerStatus;
import ee.dmgc.simplebank.exception.CustomerNotFound;
import ee.dmgc.simplebank.repository.CustomerRepository;
import ee.dmgc.simplebank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {


    private final CustomerRepository customerRepository;
    private PasswordEncoder bcryptEncoder;


    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, PasswordEncoder bcryptEncoder) {
        this.customerRepository = customerRepository;
        this.bcryptEncoder = bcryptEncoder;
    }

    @Override
    public Customer saveCustomer(Customer customer) {
        Customer customerSaved = new Customer();
        customerSaved.setPersonalId(customer.getPersonalId());
        customerSaved.setPassword(bcryptEncoder.encode(customer.getPassword()));
        customerSaved.setAccounts(new ArrayList<>());
        customerRepository.save(customerSaved);
        return customerSaved;
    }

    @Override
    public List<Customer> findAllCustomers() {
        List<Customer> customerList = customerRepository.findAll();
        return customerList;
    }

    @Override
    public List<Customer> findCustomersByFirstName(String firstName) {
        List<Customer> customerList = customerRepository.findAllByFirstName(firstName);
        return customerList;
    }

    @Override
    public Customer findCustomer(Long customerId) throws CustomerNotFound {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new CustomerNotFound("There is no any customer with id: " + customerId));
        return customer;
    }

    @Override
    public Customer findCustomerByPersonalId(String personalId) {
        return customerRepository.findByPersonalId(personalId);
    }

    @Override
    public Customer setStatus(Long customerId, String status) {
        Customer customer = customerRepository.findById(customerId).get();
        customer.setStatus(defineStatus(status));
        customerRepository.save(customer);
        return customer;
    }

    private CustomerStatus defineStatus(String status) {
        switch (status) {
            case "OPENED":
                return CustomerStatus.OPENED;
            case "ACTIVE":
                return CustomerStatus.ACTIVE;
            case "CLOSED":
                return CustomerStatus.CLOSED;
            default:
                return CustomerStatus.BLOCKED;
        }
    }
}


