package ee.dmgc.simplebank.controller;

import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.domain.model.JwtRequest;
import ee.dmgc.simplebank.domain.model.JwtResponse;
import ee.dmgc.simplebank.dto.CustomerDTOSignUp;
import ee.dmgc.simplebank.exception.CustomerNotFound;
import ee.dmgc.simplebank.service.CustomerService;
import ee.dmgc.simplebank.service.JwtUserDetailsService;
import ee.dmgc.simplebank.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class CustomerController {

    private CustomerService customerService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService userDetailsService;

    @Autowired
    public CustomerController(CustomerService customerService, AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, JwtUserDetailsService userDetailsService) {
        this.customerService = customerService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping(value = "customers/findAll")
    public ResponseEntity<List<Customer>> getAll() {
        try {
            List<Customer> customerList = customerService.findAllCustomers();
            return new ResponseEntity(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "customers/{firstName}")
    public ResponseEntity<List<Customer>> findByFirstName(@PathVariable("firstName") String firstName) {
        try {
            List<Customer> customerList = customerService.findCustomersByFirstName(firstName);
            return new ResponseEntity(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "customer/{id}")
    public ResponseEntity<Customer> findById(@PathVariable("id") Long id) {
        try {
            Customer customer = customerService.findCustomer(id);
            return new ResponseEntity(customer, HttpStatus.OK);
        } catch (Exception | CustomerNotFound e) {
            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
        }
    }



    @PutMapping(value = "customer/{customerId}/activate")
    public ResponseEntity activate(@PathVariable Long customerId, @RequestBody String status) {
        try {
        Customer customer = customerService.setStatus(customerId, status);
        return new ResponseEntity(customer, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "customer/{customerId}/block")
    public ResponseEntity close(@PathVariable Long customerId, @RequestBody String status) {
        try {
        Customer customer = customerService.setStatus(customerId, status);
        return new ResponseEntity(customer, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody CustomerDTOSignUp customerDTO) throws Exception {
        return ResponseEntity.ok(userDetailsService.save(customerDTO));
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getPersonalId(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getPersonalId());

        final String token = jwtTokenUtil.generateToken(userDetails);

        final Long userId = customerService.findCustomerByPersonalId(authenticationRequest.getPersonalId()).getId();
        JwtResponse jwtResponse = new JwtResponse(token, userId);
        return ResponseEntity.ok(jwtResponse);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
