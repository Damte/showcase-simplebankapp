package ee.dmgc.simplebank.controller;

import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.exception.CustomerNotFound;
import ee.dmgc.simplebank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class AccountController {
    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/account")
    public ResponseEntity<Account> createNewAccount(@RequestBody Long customerId) {
        try {
            Account newAccount = accountService.saveAccount(customerId);
            return new ResponseEntity<>(newAccount, HttpStatus.CREATED);
        } catch (Exception | CustomerNotFound e) {
            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "customer/{id}/accounts")
    public ResponseEntity<List<Account>> findAccountsByCustomer(@PathVariable("id") Long id) {
        try {
            List<Account> accounts = accountService.findAccountsByCustomer(id);
            return new ResponseEntity(accounts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "account/{accountId}")
    public ResponseEntity findAccount(@PathVariable Long accountId) {
        try {
        Account account = accountService.findAccountById(accountId);
        return new ResponseEntity(account, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "account/{accountId}/toggleActive")
    public ResponseEntity activate(@PathVariable Long accountId) {
        try {
        Account account = accountService.setStatus(accountId);
        return new ResponseEntity(account, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


}
