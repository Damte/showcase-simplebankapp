package ee.dmgc.simplebank.controller;

import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.dto.TransactionDTO;
import ee.dmgc.simplebank.dto.TransactionDTOListTransactionsByAccount;
import ee.dmgc.simplebank.exception.*;
import ee.dmgc.simplebank.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@CrossOrigin
@RestController
public class TransactionController {

    private TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(value = "/transaction")
    public ResponseEntity<List<Account>> doTransaction(@RequestBody TransactionDTO transactionDTO) {
        List<Account> updatedAccounts = null;
        try {
            if (transactionDTO.getTransactionAmount().compareTo(BigDecimal.ZERO) <= 0) {
                throw new TransferAmountMustBeGreaterThanZero("The transfer value must be greater than 0.");
            }
            if (transactionDTO.getAccount().getAccountNumber().equals(transactionDTO.getDestinationAccount().getAccountNumber())) {
                throw new DestinationAccountCantBeSameAsOriginAccount("Destination account must be different than origin account.");
            }
                updatedAccounts = transactionService.doTransaction(transactionDTO);
        } catch (AccountNotFound accountNotFound) {
            return new ResponseEntity(accountNotFound, HttpStatus.CONFLICT);
        } catch (TransferAmountMustBeGreaterThanZero transferAmountMustBeGreaterThanZero) {
            return new ResponseEntity(transferAmountMustBeGreaterThanZero, HttpStatus.CONFLICT);
        } catch (AccountIsNotActive accountIsNotActive) {
            return new ResponseEntity(accountIsNotActive, HttpStatus.CONFLICT);
        } catch (BalanceIsLessThanTransferAmount balanceIsLessThanTransferAmount) {
            return new ResponseEntity(balanceIsLessThanTransferAmount, HttpStatus.CONFLICT);
        } catch (AboveAmountLimit aboveAmountLimit) {
            return new ResponseEntity(aboveAmountLimit, HttpStatus.FORBIDDEN);
        } catch (DestinationAccountCantBeSameAsOriginAccount destinationAccountCantBeSameAsOriginAccount) {
            return new ResponseEntity(destinationAccountCantBeSameAsOriginAccount, HttpStatus.FORBIDDEN);
        } catch (ReachedTransferLimitPerDay reachedTransferLimitPerDay) {
            return new ResponseEntity(reachedTransferLimitPerDay, HttpStatus.FORBIDDEN);
        } catch (ReachedTransferLimitPerMinute reachedTransferLimitPerMinute) {
            return new ResponseEntity(reachedTransferLimitPerMinute, HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(updatedAccounts, HttpStatus.CREATED);
    }

    @GetMapping(value = "account/{aid}/movements")
    public ResponseEntity<List<TransactionDTOListTransactionsByAccount>> checkAccountMovements(@PathVariable("aid") Long accountId) {
        try {
            List<TransactionDTOListTransactionsByAccount> transactions = transactionService.findTransactionsByAccount(accountId);
            return new ResponseEntity(transactions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
