package ee.dmgc.simplebank.domain.converter;


import ee.dmgc.simplebank.domain.model.Transaction;
import ee.dmgc.simplebank.dto.TransactionDTOListTransactionsByAccount;
import org.springframework.stereotype.Component;

@Component
public class TransactionConverter {


    public TransactionDTOListTransactionsByAccount transactionToDTO(Transaction transaction) {
        TransactionDTOListTransactionsByAccount dto = new TransactionDTOListTransactionsByAccount();
        dto.setOriginAccountNumber(transaction.getAccount().getAccountNumber());
        dto.setDestinationAccountNumber(transaction.getDestinationAccount().getAccountNumber());
        dto.setTransactionAmount(transaction.getTransactionAmount());
        dto.setTransactionDateTime(transaction.getTransactionDateTime());
        dto.setDescription(transaction.getDescription());
        dto.setOriginAccountId(transaction.getAccount().getId());

        return dto;
    }


}

