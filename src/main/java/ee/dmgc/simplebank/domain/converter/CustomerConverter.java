package ee.dmgc.simplebank.domain.converter;


import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.domain.model.CustomerStatus;
import ee.dmgc.simplebank.dto.CustomerDTOSignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Component
public class CustomerConverter {

    private PasswordEncoder bcryptEncoder;

    @Autowired
    public CustomerConverter(PasswordEncoder bcryptEncoder) {
        this.bcryptEncoder = bcryptEncoder;

    }

    public Customer dtoToCustomer(CustomerDTOSignUp dto) {
        Customer customer = new Customer();
        customer.setFirstName(dto.getFirstName());
        customer.setLastName(dto.getLastName());
        customer.setPassword(bcryptEncoder.encode(dto.getPassword()));
        customer.setDateOfBirth(dto.getDateOfBirth());
        customer.setPersonalId(randomNumeric(10));
        customer.setStatus(CustomerStatus.ACTIVE);

        return customer;
    }


}

