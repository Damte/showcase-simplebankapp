package ee.dmgc.simplebank.domain.generator;

import com.github.javafaker.Faker;
import ee.dmgc.simplebank.domain.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static ee.dmgc.simplebank.domain.model.CustomerStatus.ACTIVE;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;


@Component
public class CustomerGenerator {
    private static final Logger log = LoggerFactory.getLogger(CustomerGenerator.class);
    private final Faker faker = new Faker();
    @Autowired
    private PasswordEncoder bcryptEncoder;

    public List<Customer> generate() {
        log.info("Starting to generate customers");
        ArrayList<Customer> customers = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            customers.add(createCustomer(i));
        }
        log.info("Customers successfully generated");
        return customers;
    }

    private Customer createCustomer(long id) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setPersonalId(randomNumeric(10));
        customer.setFirstName(faker.name().firstName());
        customer.setLastName(faker.name().lastName());
        customer.setPassword(bcryptEncoder.encode("11112222"));
        customer.setDateOfBirth(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        customer.setStatus(ACTIVE);
        return customer;
    }
}
