package ee.dmgc.simplebank.domain.generator;

import ee.dmgc.simplebank.domain.model.Account;
import ee.dmgc.simplebank.domain.model.Customer;
import ee.dmgc.simplebank.exception.CustomerNotFound;
import ee.dmgc.simplebank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AccountGenerator {

    @Autowired
    private AccountService accountService;

    public String generateAccountNumber(){
        String start = "EE";
        Random value = new Random();
        int num;
        for (int i =0; i < 14;i++) {
            num = value.nextInt(10);
            start += num;
        }
        String result = start.replaceAll("([\\d]{2})([\\d]{4})([\\d]{4})([\\d]{4})", "$1 $2 $3 $4");
        return result;
    }

    public Account generateAccount(Customer customer) {
        Account account = new Account();
//        account.setAccountNumber(generateAccountNumber());
//        //At the moment I'm giving 50 complimentary EUR to any new customer in order to be able to perform some transactions.
//        account.setCurrentBalance(BigDecimal.valueOf(100000));
//        account.setActive(true);
//        account.setCustomer(customer);
//        account.setCreatedAt(LocalDateTime.now());
//        account.setUpdatedAt(LocalDateTime.now());

        try {
            accountService.saveAccount(customer.getId());
        } catch (CustomerNotFound customerNotFound) {
            customerNotFound.printStackTrace();
        }
        return account;
    }
}
