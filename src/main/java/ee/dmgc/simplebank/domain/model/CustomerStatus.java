package ee.dmgc.simplebank.domain.model;

public enum CustomerStatus {
    OPENED, BLOCKED, ACTIVE, CLOSED
}
