# Simple Bank app

* A very simple project to display some of my code using java, spring-boot (web, data-jpa, security, junit5)

* To start the back-end server, run either `DMGCSimpleBankingApplication` or `gradle bootRun` from the command line
* The server is on port 8080
* There is an h2 db configured, with username= sa and password 123
* There it will be found the tables that represent a simple structure consisting on a customers table, an accounts table (which relation with the customers table is many to one) and lastly a transactions table (that will keep records of two different accounts via foreign key, the origin and the destination)
* Since I was thinking the task from the point of view of the customer I decided to recreate the sign up and log in process, for that I included security with Spring using JWT.
* It is possible to test the different API requests defined on the controller layer using Postman
* It should be kept in mind, that other than 'http://localhost:8080/register' and 'http://localhost:8080/authenticate', REST apis would need the token to be pased in Authorization header.
* I've kept the structure in separated layers Entity-Repository-Service-Controller and dealt with some of the most obvious exception handling, although I can already see how some entities/ functionalities could be expanded, for example I would have liked to add additional checks (like age filter),
  also some options for Admin to review transactions and permissions.
